package ru.anenkov.tm.exception.user;

import ru.anenkov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
