package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateLastNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-last-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME: ");
        @NotNull final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
