package ru.anenkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataJsonClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON CLEAR]");
        @NotNull final File file = new File(DataConst.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
