package ru.anenkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "Data-bin-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Delete bin file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        @NotNull final File file = new File(DataConst.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
