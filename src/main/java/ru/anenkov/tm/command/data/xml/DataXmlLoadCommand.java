package ru.anenkov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.Domain;
import ru.anenkov.tm.enumeration.Role;

import java.io.FileInputStream;

public class DataXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Data XML load";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        @NotNull final String path = DataConst.FILE_XML;
        if (path == null) return;
        @NotNull final FileInputStream fileInputStream = new FileInputStream(path);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
