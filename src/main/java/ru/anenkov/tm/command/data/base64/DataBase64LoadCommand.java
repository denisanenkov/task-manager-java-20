package ru.anenkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.Domain;
import ru.anenkov.tm.enumeration.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load base64 date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
