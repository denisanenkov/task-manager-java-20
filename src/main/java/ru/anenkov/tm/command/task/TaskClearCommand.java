package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

public class TaskClearCommand extends AbstractCommand {
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
