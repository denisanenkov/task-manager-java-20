package ru.anenkov.tm.command;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.service.ServiceLocator;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable public Role[] roles() {
        return null;
    }

    @Nullable public abstract String arg();

    @Nullable public abstract String name();

    @Nullable public abstract String description();

    public abstract void execute() throws Exception;

}
