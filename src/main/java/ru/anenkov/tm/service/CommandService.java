package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.service.ICommandService;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
