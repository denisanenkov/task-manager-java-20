package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IRepository;
import ru.anenkov.tm.api.service.IService;
import ru.anenkov.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    protected AbstractService(@NotNull IRepository<T> repository) {
        this.repository = repository;
    }

    public void load(@Nullable final List<T> t) {
        if (t == null) return;
        repository.load(t);
    }

    public void load(@Nullable final T... t) {
        if (t == null) return;
        repository.load(t);
    }

}
