package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task>{

    @Nullable
    List<Task> findAll(@Nullable String userId);

    void create(@Nullable String userId, @NotNull String name);

    void create(@Nullable String userId, @NotNull String name, @NotNull String description);

    void add(@Nullable String userId, @NotNull Task task);

    void remove(@Nullable String userId, @NotNull Task task);

    void clear(@Nullable String userId);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@Nullable String userId, @NotNull String name);

    @Nullable
    Task findOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Task removeOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Task updateTaskById(@Nullable String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    void load(@Nullable List<Task> tasks);

    void load(@Nullable Task... tasks);

    @Nullable
    List<Task> getList();

}
