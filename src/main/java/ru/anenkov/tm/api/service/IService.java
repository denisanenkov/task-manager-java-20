package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.AbstractEntity;

import java.util.List;

public interface IService <T extends AbstractEntity> {

    void load(@Nullable List<T> t);

    void load(@Nullable T... t);

    @Nullable
    List<T> getList();

}
