package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    @Nullable
    List<AbstractCommand> getCommandList();

}