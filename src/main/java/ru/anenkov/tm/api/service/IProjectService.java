package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface IProjectService extends IService<Project>{

    @Nullable
    List<Project> findAll(@Nullable String userId);

    void create(@Nullable String userId, @NotNull String name);

    void create(@Nullable String userId, @NotNull String name, @NotNull String description);

    void add(@Nullable String userId, @NotNull Project project);

    void remove(@Nullable String userId, @NotNull Project project);

    void clear(@Nullable String userId);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@Nullable String userId, @NotNull String name);

    @Nullable
    Project findOneById(@Nullable String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@Nullable String userId, @NotNull String id);

    @Nullable
    Project updateProjectById(@Nullable String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @Nullable
    Project updateProjectByIndex(@Nullable String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    void load(@Nullable List<Project> projects);

    void load(@Nullable Project... projects);

    @Nullable
    List<Project> getList();

}