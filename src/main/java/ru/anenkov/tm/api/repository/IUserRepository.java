package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeUser(@NotNull User user);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByEmail(@NotNull String email);

    void clear();

    void merge(@NotNull Collection<User> users);

    @Nullable
    User merge(@NotNull User user);

    void merge(@NotNull User... users);

    void load(@Nullable Collection<User> users);

    void load(@NotNull User... users);

    void load(@NotNull User user);

    @Nullable
    List<User> getList();

}
