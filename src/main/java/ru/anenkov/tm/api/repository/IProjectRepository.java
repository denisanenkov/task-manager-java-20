package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    void add(@NotNull String userId, @NotNull Project project);

    @NotNull
    void remove(@NotNull String userId, @NotNull Project project);

    @Nullable
    List<Project> findAll(@NotNull String userId);

    @NotNull
    void clear(@NotNull String userId);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Project findOneById(@NotNull String userId, @Nullable String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @Nullable String id);

    @NotNull
    void merge(@Nullable Collection<Project> projects);

    @NotNull
    Project merge(@Nullable Project project);

    @NotNull
    void merge(@Nullable Project... projects);

    @NotNull
    void clear();

    @NotNull
    void load(@Nullable Collection<Project> projects);

    @NotNull
    void load(@Nullable Project... projects);

    @NotNull
    void load(@Nullable Project project);

    @Nullable
    List<Project> getList();

}
