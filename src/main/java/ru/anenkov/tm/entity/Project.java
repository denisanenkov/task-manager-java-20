package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId = "";

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}