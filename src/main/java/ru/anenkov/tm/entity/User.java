package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.HashUtil;


import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @Nullable
    private String email = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String middleName = "";

    @Nullable
    private String lastName = "";

    @NotNull
    public boolean isLocked() {
        return locked;
    }

    @NotNull
    public void setLocked(@NotNull boolean locked) {
        this.locked = locked;
    }

    @NotNull
    private boolean locked = false;

    @NotNull
    private Role role = Role.USER;

    public String getPasswordHash() {
        return HashUtil.salt(passwordHash);
    }

}
