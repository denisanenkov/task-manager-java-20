package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IRepository;
import ru.anenkov.tm.entity.AbstractEntity;
import ru.anenkov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    private final List<T> entities = new ArrayList<>();

    public void merge(@Nullable final Collection<T> t) {
        if (t == null) return;
        for (@NotNull final T entity : t) merge(entity);
    }

    @Nullable
    public T merge(@Nullable final T t) {
        if (t == null) return null;
        entities.add(t);
        return (T) entities;
    }

    public void merge(@Nullable final T... t) {
        if (t == null) return;
        for (@Nullable final T entity : t) merge(entity);
    }

    public void load(@Nullable final Collection<T> t) {
        clear();
        merge(t);
    }

    public void load(@Nullable final T... t) {
        clear();
        merge(t);
    }

    public void load(@Nullable final T t) {
        clear();
        merge(t);
    }

    public void clear() {
        entities.clear();
    }

}
