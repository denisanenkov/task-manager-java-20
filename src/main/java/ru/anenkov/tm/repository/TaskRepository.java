package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, @Nullable final Task task) {
        task.setUserId(userId);
        if (task == null) return;
        tasks.add(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @Nullable final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @NotNull
    @Override
    public Task findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        for (@NotNull final Task task : result) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        for (@NotNull final Task task : result) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable final Task task) {
        if (!userId.equals(task.getUserId())) return;
        if (task == null) return;
        this.tasks.remove(task);
    }

    @Override
    public Task removeOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void merge(@Nullable final Collection<Task> tasks) {
        if (tasks == null) return;
        for (@NotNull final Task task : tasks) merge(task);
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void merge(@Nullable final Task... tasks) {
        if (tasks == null) return;
        for (@Nullable final Task task : tasks) merge(task);
    }

    @Override
    public void load(@Nullable final Collection<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(@Nullable final Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(@Nullable final Task task) {
        clear();
        merge(task);
    }

    @Nullable
    @Override
    public List<Task> getList() {
        return tasks;
    }

}
