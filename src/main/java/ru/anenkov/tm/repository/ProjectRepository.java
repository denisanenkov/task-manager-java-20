package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void merge(@Nullable final Collection<Project> projects) {
        if (projects == null) return;
        for (@NotNull final Project project : projects) merge(project);
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void merge(@Nullable final Project... projects) {
        if (projects == null) return;
        for (@Nullable final Project project : projects) merge(project);
    }

    @Override
    public void load(@Nullable final Collection<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(@Nullable final Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(@Nullable final Project project) {
        clear();
        merge(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Nullable
    @Override
    public List<Project> getList() {
        return projects;
    }

    @Override
    public void add(@NotNull final String userId, @Nullable final Project project) {
        project.setUserId(userId);
        if (project == null) return;
        projects.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        if (project == null) return;
        this.projects.remove(project);
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        for (@NotNull final Project project : result) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final List<Project> result = new ArrayList<>();
        if (result == null || result.isEmpty()) return null;
        for (@NotNull final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        for (@NotNull final Project project : result) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}
